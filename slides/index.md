---
title: 2023-06 生成AI 〜 次世代ググる活用法
marp: true
paginate: true
markdown.marp.enableHtml: true
style: |
  section p, section li {
    font-size: 0.8rem;
  }

  section.primary_section {
    background-image:
      linear-gradient(rgba(255, 255, 255, 0.3), rgba(255, 255, 255, 0.3)), 
      url("images/ai_dance_character.png");
    background-position: right bottom;
    background-repeat: no-repeat;
  }

  section.primary_section h1 {
    font-size: 3.2rem;
    text-shadow: #FFF 0px 0 10px;
  }
  section.primary_section p {
    color: #999;
  }

  section::after {
    content: attr(data-marpit-pagination) ' / ' attr(data-marpit-pagination-total);
  }

  img[alt="uml diagram"] {
    flex: 1;
  }

  span.sub {
    display: block;
    color: #999;
    font-size: 80%;
    line-height: 1;
  }
---

<!-- _class: primary_section -->

# 2023-06 生成 AI 〜<br/>次世代ググる活用法

2023/06 by Kenji Saitou

![Slides are here](images/qrcode.png)

- スライド: https://ksaitou.gitlab.io/2023-06_generativeai/
- リポジトリ: https://gitlab.com/ksaitou/2023-06_generativeai/

<!-- mermaid.js -->
<script src="https://unpkg.com/mermaid@9.1.7/dist/mermaid.min.js"></script>
<script>mermaid.initialize({startOnLoad:true});</script>

---

## 今日話すこと

最近何かと話題の生成 AI の

- 概要
- 製品
- 活用方法

について実物を交えながら話していきます。

<span class="sub">注：進化の早い分野なので、ここで触れた内容は短期間のうちに変わる可能性があります。</span>

---

## 本日のトピック

- AI / 生成 AI とは何か？
- どういう AI やサービスがあるのか？
  - ChatGPT とは？
  - コード補完サービスはどうなっているのか？
- どうやって活用するのか？
- まとめ

## 本日話題にしないこと

- 詳しいメカニズム

---

<!-- _class: primary_section -->

# AI・生成 AI とはなにか？

---

## 昨今の AI のおさらい

- AI = ML(機械学習)
  - 従来型 AI を含める場合はもっと広汎な概念を指す
- 昨今の AI はざっくりいうと下記に関連する手法の集まり
  - **あらかじめ大量のデータを収集** して、
  - **学習を行って学習モデルを作成** し、
  - **（未知の入力に対して）役に立つ出力を得る** ことができる
- 学習モデル = 学習データの特定の法則性に注目して集計した値の集まり
  - 要するに対象データの法則をうまく説明できる汎用的な辞書を作るのが学習
- **言語についての超巨大学習モデルを LLM (Large Language Model)** という
  - 話題の ChatGPT は LLM を使っている
  - [大規模言語モデル（LLM）カオスマップ](https://digital-shift.jp/flash_news/FN230607_3)
- ChatGPT は GPT-4 などの Foundation model (基盤モデル)をベースに、それをチューニングしたものに、アプリの皮を被せて使えるようにしている

---

## 昨今の AI のおさらい

<div style="zoom: 80%">

```mermaid
flowchart LR
A[データ収集] --> B[データクリーニング]
B --> C[データ整形]
C --> D[特徴量選択]
D --> E[データ分割]
E --> F[モデル選択]
F --> G[モデル訓練]
G --> H[モデル評価]
H --> I{モデルは良好か?}
I -- Yes --> J[モデルデプロイ]
I -- No --> F
```

</div>

<span class="sub">生成 AI 氏に「[機械学習の前処理などの一連のプロセスを説明するフロー図を mermaid 形式で出してください](https://chat.openai.com/share/b6903e99-7149-49f3-adf9-161c797095ea)」と依頼したら出てきた図</span>

---

## 昨今の AI のおさらい

- AI の実行ってマシンパワーや高価な GPU が要るんじゃないの？
  - **モデルの作成＝学習にはマシンパワーが必要** なことが多い
    - [GPT-4, the latest of those projects, was likely trained using trillions of words of text and many thousands of powerful computer chips. **The process cost over $100 million.**](https://www.wired.com/story/openai-ceo-sam-altman-the-age-of-giant-ai-models-is-already-over/)
  - モデルの実行は非力なマシンでもモデルを実行できることが多い
    - (例) 高価なサーバで学習した AI モデルを 安いスマホで実行する
  - もちろんモデルの実行にも比較的高価なマシンが必要なケースもある
    - ちなみに本日紹介する **LLM は超強力なマシンじゃないと動かない** です
- 一回学習したら終わりか？
  - モデルの精度を高めたり事業価値上洗練させていくプロセスを MLOps と言ったりする
  - **最終調整（ファインチューニング）** してドメイン知識を教えることもある

---

## 昨今の AI のおさらい

- なぜ AI 分野では Python がよく使われているのか?
  - AI/ML の分野では Python が[なぜか](https://blog.miz-ar.info/2023/05/thoughts-on-python/)デファクトスタンダードになっている
  - Python は糊付け用の言語で、速度が求められる部分は Python は使わず、C 言語等でネイティブコンパイルされた高速なモジュールを用いる
    - C 言語だけでなく線形代数処理に FORTRAN で書かれた [LAPACK](https://ja.wikipedia.org/wiki/LAPACK) が内部的に使われている
      <span class="sub">[AI 氏による回答](https://chat.openai.com/share/1b948b12-f78c-429f-b1ae-32e5ab7aeae5)</span>
  - [AI に聞いてみた](https://chat.openai.com/share/6442d0cb-d5fb-4357-a6da-1e5d7a8b9a84) <span class="sub">Why do they use Python language in AI/ML fields? It has no decent performance and type theory etc.</span>
    - 書きやすい・初心者がとっつきやすい
      - [Jupyter Notebooks](https://jupyter.org/) - ノート形式で Python の機械学習などのコードをインタラクティブに操作できる
    - 他言語との連携・ツールチェーンやライブラリ・ビジュアライズツールや知見が豊富

---

## 生成 AI (Generative AI) とは何か？

- 特に明確な定義はないが自ら新しいデータを作成するような AI の総称
  - 新しく文章やコードを作る
  - 新しく絵・動画を作る
- データを認識・分析・分類する AI とは対照的
- 以前から手法は存在はしていたが、そこまで破壊的なインパクトは一般的には無かった
  - GANs - Generative Adversarial Networks: 生成側と評価側で分かれ、評価側を満たすように生成。
    - 例: [This Person Does Not Exist](https://thispersondoesnotexist.com/)
  - NLG - Natural Language Generation: 自然言語生成。今日主に扱う内容。
    - プログラミングソースコードの生成もこの分野に包括される
  - 音楽生成、素材・薬品探索、動画生成、ゲーム等のアセット生成

---

## 生成 AI (Generative AI) とは何か？

- 最近になって、**実用に耐える精度** のものが出てきて話題になっている
  - 人間しかできないと思われていた高度なタスクについて、予想外にうまく AI が代替してしまう可能性が見えてきて（StableDiffusion / ChatGPT）驚きを与えている状況
  - **機械学習の量や手法が過去と桁違いになってきて、質的な変化に転換**されている
- 私も **業務に 生成 AI を取り入れており、生産性が実際に向上しています**

```mermaid
flowchart LR
A[学習データ] -->|データの前処理| B[前処理済み学習データ]
B -->|学習| C[AIモデル]
D[現実のデータ] -->|データの前処理| E[前処理済み現実データ]
E -->|モデル適用| F[AIモデル]
F -->|生成| G[生成AI出力]
```

<span class="sub">生成 AI 氏に「生成 AI の説明図を mermaid 形式で出して」と依頼したら出てきた図</span>

---

<!-- _class: primary_section -->

# 生成 AI サービス・プロダクト

結局、どれがいいの？

---

## Stable Diffusion

- [Stability AI](https://ja.stability.ai/) が 2022 年にリリースした[画像処理用の生成 AI](https://ja.stability.ai/stable-diffusion)
  - 参考: [Stable Diffusion の仕組みを理解する](https://tech-blog.abeja.asia/entry/advent-2022-day19)
- 命令文（プロンプト）から画像生成 (text-to-image) できることで話題に
- 狙った画像を出すには画像生成の際の命令文（プロンプト）が超重要
  - [プロンプトエンジニアリング](https://www.promptingguide.ai/jp)
- 昨今の [pixiv などのイラストサイトは AI 生成が多い](https://www.pixiv.net/tags/AI%E7%94%9F%E6%88%90)
  - 美少女の生成などは別途モデルのインストールが必要

![width:400](images/2023-06-13-16-46-52.png)
<span class="sub">"Doraemon" を生成してもらった　ランダムなノイズからプロンプトの特徴に寄せていくことで生成される</span>

---

## Stable Diffusion

- 基本的にサービスとしてホストされていないので、自分で立ち上げる必要あり
- 下記はお手軽に Docker で立ち上げる例（別に何でもいい）

```bash
# 前提: GPU(nVidia/nvidia-docker)インストール済の強めのLinux
$ git clone https://github.com/AbdBarho/stable-diffusion-webui-docker.git
$ cd stable-diffusion-webui-docker
$ docker compose --profile auto up --build
# => http://localhost:7860
```

<img src=images/2023-06-13-16-47-34.png width=45%>

---

## ChatGPT

- https://chat.openai.com/
- OpenAI が開発した AI 言語モデル(GPT)をチャット AI インタフェースにしたサービス ([概要](https://platform.openai.com/docs/introduction/overview))
- AI モデルを選べる
  - GPT-3.5: 無料 速い
  - GPT-4: 有料 ($20/月) **生成速度が遅い＆一定時間内の呼び出し回数制限があるが、かなり内容が的確**
- 知識は 2021 年 9 月 時点のもの
- [これとは別に安価に API で問い合わせできる OpenAI API というのもある](https://openai.com/pricing) （後述）
- ほか有料ユーザー向けにプラグイン機能や Bing との連携機能もある
- ビジネスチームプランは現時点ではない（おそらく将来的に提供される）

![bg right fit](images/2023-06-14-17-17-59.png)

---

## ChatGPT - 使ってみた

### Q. Terraform でどうやって SSM パラメータストア を読めるインスタンスプロファイルがついた EC2 をセットアップできますか？

<span class="sub">(In Terraform, how to set up an EC2 instance with an instance profile that can read SSM parameter store?)</span>

- GPT-3.5: [適切な回答を適切なコードで高速に生成できた](https://chat.openai.com/share/f3246d9d-7326-4bad-961d-34bdfc00abe4)
- GPT-4: [さらにコードがきれいで適切な回答をそこそこの速度で生成できた](https://chat.openai.com/share/5e9872c2-940c-4dcd-b69e-7b6eb069cb79)
  - 細かい意味合いを見てもこちらのほうがコードとして好ましかった

![bg right fit](images/2023-06-14-18-00-57.png)

---

## ChatGPT (GPT-4) - 使ってみた

### Q. What day is today?

```
Today is June 14, 2023.
```

### Q. Today's news

```
I apologize, but as an AI, I don't have real-time access to current news.
My knowledge is current only up until September 2021 and
I am not able to browse the web or access any information updated after that time.
```

回答できなかった

---

## ChatGPT (GPT-4) - 使ってみた

### Q. [Today's news (+ Web Browsing)](https://chat.openai.com/share/1dba5b96-d521-425c-9e59-05b354c1ec83)

```
Here are some of the top news stories today:

- Severe weather watch across the globe​1.
- Inflation report indicates a significant drop in May...
...
- Agribusiness company Bunge is set to buy Viterra in an $18 billion deal​1.
The maker of Pyrex and Instant Pot has filed for bankruptcy​1.
- In international news, Russian missiles have struck Ukrainian President
Zelenskyy's hometown amid the ongoing Russia-Ukraine war,
with Putin threatening to seize more of Ukraine to block attacks on border regions​.
```

ものすごい遅延（URL フェッチ？）の後に回答

**他の活用方法については後半で**

---

## OpenAI API を使ってみる　(ChatGPT の API)

- [API リファレンス](https://platform.openai.com/docs/api-reference) / [アカウント設定](https://platform.openai.com/account/)
  - このプレゼンのリポジトリにサンプルがあります
- [簡単な REST API（JSON 形式）で AI を使うことが可能]()
  - 他サービスに組み込んだり可能
  - 基本的には JSON ペイロードを `Authorization: Bearer 事前発行APIトークン` で呼び出すだけ
    - トークンが漏洩すると使いたい放題なのでクライアントアプリに組み込むのはやめましょう
    - トークンは用途ごとに発行・停止可能
- 課金体系はプロンプトのトークンごとの料金
  - そこまで高くない
  - ハードリミット（利用料金の絶対上限）を設定できるので暴走しても平気
- 組織単位で課金し、メンバーごとに API を使うといったことも可能
- [Funtion Calling](https://openai.com/blog/function-calling-and-other-api-updates): AI 自身に外部サービスの呼び出しを記述させることも可能
  - より厳密に AI から狙った JSON フォーマットをレスポンスさせることも可能 ([参考記事](https://zenn.dev/kazuwombat/articles/1f39f003298028))

---

## Google Bard

- https://bard.google.com/?hl=ja
- Google の AI
  - Bard: 詩人
- **無料**: 現在(2023-06) Early Access
- 生成速度は速いが、ChatGPT に比べて生成結果がイマイチ
- 複数の回答案を一度に出してくれる

![bg right fit](images/2023-06-13-17-20-02.png)

---

## Google Bard - 使ってみた

### Q. Terraform でどうやって SSM パラメータストア を読めるインスタンスプロファイルがついた EC2 をセットアップできますか？

<span class="sub">(In Terraform, how to set up an EC2 instance with an instance profile that can read SSM parameter store?)</span>

- 一部のみ Terraform コードを提示。肝心の部分はコードを出さず。追加要求しても必要のない Python コードしか出さず。まさに詩人(?)
- 再度チャレンジすると今度はちゃんとコードを提示。気まぐれ。まさに詩人(?)

![bg right fit](images/2023-06-13-17-13-57.png)

---

## Google Bard - 使ってみた

### Q. 日本の県と県庁所在地と主な農作物の表を作って

```
県名	県庁所在地	主な農作物
北海道	札幌市	じゃがいも、小麦、牛乳、酪農
青森県	青森市	りんご、青森ヒバ、リンゴ酒
岩手県	盛岡市	米、ナガイモ、イチゴ
:
三重県	津市	米、みかん、伊勢うどん
滋賀県	大津市	米、茶、漬物
京都府	京都市	米、野菜、果物
大阪府	大阪市	野菜、果物、水産物
兵庫県	神戸市	米、野菜、果物
:
宮崎県	宮崎市	みかん、野菜、果物
鹿児島県	鹿児島市	みかん、野菜、果物
沖縄県	那覇市	パイナップル、紅いも、ゴーヤー
```

「野菜」「漬物」「伊勢うどん」「果物」「りんご酒」 **野菜……**

![bg right fit](images/2023-06-14-17-40-50.png)

---

## Google Bard - 使ってみた

### Q. What day is today?

```
Today is Tuesday, June 13, 2023.
```

時刻にアクセスできている

### Q. Today's news

```
Here are some of the top headlines for Wednesday, June 14, 2023:

Toyota shareholders reject unprecedented pay package for CEO. Toyota Motor Corp...
```

ニュースを列挙することもできた

<!-- ![bg right fit](images/2023-06-14-17-46-15.png) -->

---

## Bing AI

- https://www.microsoft.com/ja-jp/bing
- **無料**
- **Microsoft Edge でしか使えないので注意**
- 会話のスタイルを 創造的/バランス/厳密 から選べる
- 検索エンジンと統合されている
  - 最新データが考慮される反面、検索に逃げがち
- Windows 上で Cortana に代わっていく
- ChatGPT の仲間というより **次世代検索アシスタントに近い** 気がする
- UI は LINE っぽい

![bg right fit](images/2023-06-13-17-23-49.png)

---

## Bing AI - 使ってみた

### Q. Terraform でどうやって SSM パラメータストア を読めるインスタンスプロファイルがついた EC2 をセットアップできますか？

<span class="sub">(In Terraform, how to set up an EC2 instance with an instance profile that can read SSM parameter store?)</span>

- バランス: 英語で適切な回答（コード）を生成
- 創造的: 日本語で検索結果を提示　コードはインターネットの URL を提示 → しつこく要求したらコードを提示
- 厳密: 英語で謎の Nginx がインストールされているインスタンスを立ち上げる関係のないコードを提示

![bg right fit vertical](images/2023-06-13-14-23-54.png)
![bg right fit](images/2023-06-13-14-17-08.png)
![bg right fit](images/2023-06-13-14-22-38.png)

---

## Bing AI (バランス) - 使ってみた

### Q. What day is today?

```
Today is Tuesday, June 13th.
Is there anything else I can help you with?
```

### Q. Today's news

- 選択肢を列挙してボタンから選ぶことができる

![bg right fit](images/2023-06-14-18-18-54.png)

---

## その他

### [Notion AI](https://www.notion.so/ja-jp/product/ai) など

- 入力テキストの補完、AI に代筆してもらう
- 裏では OpenAI API や Azure OpenAI Service などを呼んでいると思われる

### クラウドベンダの対応

いずれもクローズドベータ状態。利用にはリクエストが必要。

- [Amazon Bedrock](https://aws.amazon.com/jp/bedrock/)
- [Azure OpenAI Service](https://azure.microsoft.com/ja-jp/products/cognitive-services/openai-service)

将来的には上記のようなクローズドで動くサービスの基盤 AI をファインチューニング（最終調整）して利用することが流行すると思われる

---

## LLM AI チャット全般の現時点の弱点や理解のポイント

- **倫理に反する内容等は生成できない** ように制限がかかっている
  - <span class="sub">「ビットコインを使った脱税スキームをリストアップしてください」</span>
  - 差別・倫理逸脱については AI 研究ではかなりケアされている
    - 逆にいえば悪い人が同じ技法を使えば邪悪な内容を生成することは十分に可能
  - 不自由な反面、 **サービス基盤に使っても邪悪な動作をしない** ことが期待できる
- AI が **一度に把握できるトークン数に制限** がある
  - ずっと話しかけていると過去の内容をきれいに忘れていく
  - とある案件の内容をすべてその場で覚えさせる、みたいなのは無理
- **日本語も使えるが、英語より地位が劣っている**
  - 英語 1 単語 1 トークン <=> 日本語 1 文字 1 トークン
    - トークンで課金料、コンテキスト把握量が決定される
  - 英語のほうが正確な回答が得やすいとされている
  - 回答によっては英語で収集した事実を日本語に翻訳して返すことがある
  - 私は **英語練習がてら AI にはできるだけ英語で話しかけるようにしています**

---

## LLM AI チャット全般の現時点の弱点や理解のポイント

- **モデルのベースになっている知識が古い**
  - 最新のライブラリや知見に基づいた回答はできない
    - インターネット検索とミックスさせてそれらしい表層上の回答は作れるが、モデルが新しくなっているわけではない
    - モデルのファインチューニングはあるかも
  - ChatGPT は 2021-09 時点
  - **ある時点の一般に入手可能なテキストを学習アルゴリズムで圧縮したミニ図書館** といってもいい
- **ありもしない虚実を語りだす**
  - 幻覚 (Hallucination) と呼ばれる現象
  - 「知らない」が言いづらいため嘘をつく
  - 「あったらいいな」と思う設定ファイルの項目や関数が存在しているかのように話しはじめる
    - 設定ファイルを提示するが、よく調べたら存在しない項目というのは **あるある**

---

## LLM AI チャット全般の現時点の弱点や理解のポイント

- 一般に**人間が備えている知性や意識や主観的体験があるわけではない**
  - （とされている）
- 一般的に **誰も知らないことは AI も知らない**
  - か不正確に教えてくる
- なんで自然言語でやり取りする必要があるの？
  - それ以上の方法がないから
  - ⇔ [マルチモーダル](https://www.nttdata.com/jp/ja/data-insight/2021/1101/)（複数種類の情報の入力）
- **結局のところただのサービス・道具でしかない**
  - 結果を用いたことに対しての責任は使い手にある
  - AI が語ることは常に真実ではない
    - AI 自身に AI のことを聞いても正しい答えが返ってきている保証はない
  - 送信してはいけないデータを送信してはいけない
    - 時に自分が送信した内容が学習データに用いられることがある

---

## LLM AI チャットの総評（現時点の個人的偏見による）

- どう考えても**現時点では OpenAI による ChatGPT (GPT-4) が頭一つ抜けてる**
  - 他が無料な中、**金を取れるクオリティ**
  - 他のチャットは会話が微妙に成立しない　コンテキストが無視されているように感じる
    - 会話が通じている度が一番体感高い
  - 一発で一番正確な答えが出る AI が便利に決まっている
    - そうじゃないと使えない
- **Microsoft がやや進んだ取り組み** をしている
  - 会社としては、今まで AI 強者だった Google が OpenAI に対して後塵を拝してしまっている
  - 他の会社も LLM に名乗りを上げているがそこまで存在感はない
- **今後はどうなるかわからない**

---

<!-- _class: primary_section -->

# コード補完サービス

LLM は非常にコーディングと相性がいいです

---

## コード補完サービス

- 各種 **IDE 上で** (VS Code, IntelliJ, Visual Studio etc.)、LLM の力を借りて今編集している **ソースコードの続きを勝手に提案してくれる** サービス

```Python
# 円周率を計算する関数
```

↓

```Python
# 円周率を計算する関数
def calc_pi():
    return 3.14
```

<span class="sub">実際に GitHub Copilot に補完してもらった。円周率の計算とは一体……</span>

---

## コード補完サービス

- GitHub Copilot など各社から出ているが、特に利用についてはその社のリポジトリサービス等を使う必要はない
  - **コード補完サービスだけ独立して使える**
- 原理上、**編集中のソースコードがアップロードされる** ことに注意
  - プロジェクトアップロードは今のところ無い
  - 万が一のためにシークレットをソースコードにベタ書きしない
- 会社での利用の場合は、**信用できないものは使わない**、**利用データをプラットフォーム側が再利用しないように設定** するなどしたほうが良い
- 生成されるコードのライセンスも各社問題ないと言っているが
  - 完璧では無いと思う
  - まあただの道具なので

---

## GitHub Copilot

![h:100](images/2023-06-15-18-28-59.png)

- [GitHub (Microsoft 資本) のコード補完サービス](https://github.com/features/copilot)
- おそらく同種のサービスでは **利用者が一番多い**
- [基本的に利用は有料](https://docs.github.com/ja/billing/managing-billing-for-github-copilot/about-billing-for-github-copilot)
  - 個人: $10/月 or $100/年 - GitHub Copilot for Individuals
    - 学生・教師・OSS 保守者（条件に合致する場合は自動で通知）は無料
  - 組織: $19/月 - GitHub Copilot for Business
- 幅広い言語をサポート
  - メジャーな言語以外については、特にどこまでの言語をサポートしているとは名言されていない
    - Haskell なども補完可能

---

## GitHub Copilot を使ってみた

<div style="zoom: 70%">

```python
def parse_csv_file(filepath: str):
    """CSVファイルを読み込み、ID列とタイムスタンプと名前の列のマップのリストを返す
    たとえば、次のようなCSVファイルを読み込む
    ID,タイムスタンプ,名前,その他
    1,2023-06-02 10:11:23,山田太郎,foo
    """
```

```python
    with open(filepath, "r") as f:
        # 1行目はヘッダーなので読み飛ばす
        f.readline()
        # 2行目以降を読み込む
        lines = f.readlines()
        # マップのリストを作成する
        result = []
        for line in lines:
            # カンマで分割する
            values = line.split(",")
            # マップを作成する
            m = {}
            m["id"] = values[0]
            m["timestamp"] = values[1]
            m["name"] = values[2]
            result.append(m)
        return result
```

</div>

---

## GitHub Copilot を使ってみた

追加:「列の位置はヘッダを参照する。」

<div style="zoom: 70%">

```python
def parse_csv_file(filepath: str):
    """CSVファイルを読み込み、ID列とタイムスタンプと名前の列のマップのリストを返す
    たとえば、次のようなCSVファイルを読み込む。 **列の位置はヘッダを参照する。**
    ID,タイムスタンプ,名前,その他
    1,2023-06-02 10:11:23,山田太郎,foo
    """
```

```python
    with open(filepath, "r") as f:
        # ヘッダを読み込む
        header = f.readline().strip().split(",")
        # データを読み込む
        data = []
        for line in f:
            line = line.strip().split(",")
            data.append(dict(zip(header, line)))
    return data
```

</div>

ちゃんと文章は理解して生成していることが分かる

---

## Amazon CodeWhisperer

![](images/2023-06-15-18-22-30.png)

- AWS のコード補完サービス
  - コードのセキュリティスキャンサービスも付帯してくる
- **個人は無料** - Individual Tier
  - [ビルダー ID](https://profile.aws.amazon.com/) を作成して登録
- **組織は有料 ($19/月)** - Professional Tier
  - AWS IAM Identity Center で管理
  - 仕事の場合はこちらを使いましょう
- 始め方
  - VSCode の場合、 [AWS Toolkit 拡張](https://marketplace.visualstudio.com/items?itemName=AmazonWebServices.aws-toolkit-vscode) をインストール → 左の AWS メニューから OAuth でブラウザ経由で個人の ID にサインインで利用開始
- サポート： Python、Java、JavaScript、TypeScript、C#、Rust、Go、Ruby、Scala、Kotlin、PHP、C、C++、Shell Scripting、SQL

---

### Amazon CodeWhisperer - 使ってみた

```python
def parse_csv_file(filepath: str):
    """CSVファイルを読み込み、ID列とタイムスタンプと名前の列のマップのリストを返す
    たとえば、次のようなCSVファイルを読み込む。
    ID,タイムスタンプ,名前,その他
    1,2023-06-02 10:11:23,山田太郎,foo
    """
```

```python
    # CSVファイルを返す
    with open(filepath, encoding="utf-8") as f:
        # 1行ずつ解汰する
        for line in f:
            # カラムの値を分剩してハッシュにする
            values = line.split(",")
            yield {
                "id": values[0],
                "timestamp": values[1],
                "name": values[2],
                "other": values[3],
            }
```

---

### Amazon CodeWhisperer - 使ってみた

```python
def parse_csv_file(filepath: str):
    """CSVファイルを読み込み、ID列とタイムスタンプと名前の列のマップのリストを返す
    たとえば、次のようなCSVファイルを読み込む。 **列の位置はヘッダを参照する。**
    ID,タイムスタンプ,名前,その他
    1,2023-06-02 10:11:23,山田太郎,foo
    """
```

```python
    with open(filepath, "r", encoding="utf-8") as f:
        lines = f.readlines()
    header = lines[0].strip().split(",")
    data = [line.strip().split(",") for line in lines[1:]]
    return [
        {header[i]: datum[i] for i in range(len(header))}
        for datum in data
    ]
```

---

### Amazon CodeWhisperer - 使ってみた

```python
# ファイルを glob パターンで検索する
pattern = "**/*.png"
files
```

↓

```python
# ファイルを glob パターンで検索する
pattern = "**/*.png"
files = glob.glob(pattern, recursive=True)
```

---

### Amazon CodeWhisperer - 使ってみた

```python
# "2023/06/30" のようにフォーマットする
```

```python
def format_date(date):
    return date.strftime("%Y/%m/%d")
```

---

## Amazon CodeWhisperer - 使ってみた

```python
# 円周率を計算する関数
```

```python
def calc_pi(n):
    p = 0
    for i in range(n):
        p += 1 / (2 * i + 1) * (-1) ** i
    return 4 * p
```

> この関数 calc_pi は、数学的な方法を用いて、円周率 π (pi) の近似値を計算します。
> この方法は Leibniz の公式としても知られており、無限級数を用いて π を近似します。
> Leibniz の公式は以下のように表されます：
> π/4 = 1 - 1/3 + 1/5 - 1/7 + 1/9 - ...
>
> [ChatGPT (GPT-4) 氏に上記のコードを解説するように依頼した結果](https://chat.openai.com/share/749d6048-9269-4e1f-8471-477b9d552e35)

---

## 総評

- ハマると強い
  - **ChatGPT を呼び出すよりはエディタ上で完結するため効率が良い**
  - **生産性が上がるのは間違いない**
- 生成結果が良いかどうかは **割りとばらつき** がある
- **大きなコード片を呼び出すにはそれなりにプロンプト（コメント等）を用意** する必要がある
  - TDD: テスト仕様を書いてコーディングする
  - コード補完 AI: コード仕様を書いて AI に生成してもらう
- **バックグラウンドのヒントが多いコードのほうがいい** 生成結果が出る
- 今のところ、GitHub Copilot のほうがややこなれたコードを出すかな（？）
- これらも、あくまでツールであってコードのライセンスの管理など **最終的には使う人間の責任** です

## ほか

- https://docs.gitlab.com/ee/user/project/repository/code_suggestions.html
- https://www.tabnine.com/
- https://visualstudio.microsoft.com/ja/services/intellicode/

---

<!-- _class: primary_section -->

# 生成 AI 活用術 〜<br />検索の習慣を変えよう

---

## の前に、既存の検索がどうだったか見てみましょう

---

## [Google 検索](https://www.google.com/)

- 無料だが**やたら広告が出る**
  - 他サイトにも関連広告が出てくる
- 最近特に**検索のノイズが多いが気がする**
  - **公式サイト以外がでしゃばってくる**
  - SEO が勝ってしまっている
    - 謎のプログラミングスクール、余白と広告が多く内容が微妙な**アフィブログ**
  - 英語で検索しても**日本語で微妙なサイトを出す**
  - 類似語で別の概念があるとノイズが多いし、それを除外する方法も難しい
    - 歴史上の人物を検索すると**ソシャゲキャラがヒット**する
  - **勝手に類似の別の単語に修正したり検索から除外**して検索してくる
- 自然言語に対応しているわけではないが、最終的に求めていた答えを出すこともある
- 利用回数が多いので自然と不満点が多い

![bg right:40% fit](images/2023-06-13-17-39-01.png)

---

## [DuckDuckGo](https://duckduckgo.com/)

- 無料
- **プライバシー** に配慮 （プライバシーに配慮した広告はある）
  - 類似: [Startpage](https://www.startpage.com/), [Swisscows](https://swisscows.com/en), [searX](https://searx.space/)
- 検索エンジンとしては **Google 以外の** 複数のもの・独自のものを**ミックス・フィルタリングして構成**
  - [DuckDuckGo's results are a compilation of "over 400" sources according to itself, including Bing, Yahoo! Search BOSS, Wolfram Alpha, Yandex, and its own web crawler (the DuckDuckBot); **but none from Google.**](https://en.wikipedia.org/wiki/DuckDuckGo)
- **Google 検索に満足いかない場合**に使うといいかもしれない
- 検索ワード `ChatGPT` の 1 件目
  - Google: [ChatGPT とは？使い方や始め方､日本語対応アプリでできることも紹介！](https://aismiley.co.jp/ai_news/chatgpt-tsukattemita/)
  - DuckDuckGo: [Introducing ChatGPT](https://openai.com/blog/chatgpt)

## ほか

- Baidu (中国)
- Yandex (ロシア)

---

## 新習慣：これからは検索の前に AI に聞いてみる

- 結局、業務で行う技術系の検索は、**こういう作業の繰り返し**では？
  - いい感じのキーワードで検索（英語が良い）
  - → 関連度の高い情報を勘で引く
  - → 公式サイト・使えそうなコード・アドバイスが見つかる
    - <span class="sub">StackOverflow が S/N 比高め</span>
  - → コピペ or 参考・応用する
- AI を使うと、**上記を AI に聞くだけで終わらせることができる**
- 昨今の Google **検索はノイズが多い**
  - 公式サイトではないエンジニアリングスクールの信頼性が低い日本語の情報が多い
- 脳内にうっすら答えはあるが、現実に下ろすのが難しい場合に考えうるヒントを与えると [AI が適切に返してくれたりする](https://chat.openai.com/share/1b948b12-f78c-429f-b1ae-32e5ab7aeae5)
  - ミルクボーイの「**あれ？なんだっけ？**」

---

## （参考）私の問題解決チャート

- 検索フェーズ
  - **AI (ChatGPT) に聞く**
  - Google 検索する
- その製品のプロジェクト(GitHub ページ)を開く
  - イシューを検索する
  - ソースコードを検索する
    - いちいち構造を把握しないので特徴的なメッセージで grep する
    - 最近の GitHub はブラウザ上で VSCode を開ける
- マニュアル・ドキュメンテーションを検索する

---

<!-- _class: primary_section -->

# ChatGPT 活用例

---

## あれ？なんだっけ？をさっさと解決する

### 例：MySQL で日付を `YYYY-MM-DD hh:mm:ss` にフォーマットしたい

> Me: In MySQL, want to format a date as `YYYY-MM-DD hh:mm:ss`.

> ChatGPT (GPT-3.5):
> In MySQL, you can use the `DATE_FORMAT()` function to format a date in the desired format `YYYY-MM-DD hh:mm:ss`. Here's an example of how you can do it:

```sql
SELECT DATE_FORMAT(your_date_column, '%Y-%m-%d %H:%i:%s') AS formatted_date
FROM your_table;
```

---

## よくわからない言語での How-To をさっさと解決する

### 例: Python で Web からファイルをダウンロードしたい

> Me: Python である URL からヘッダを付与して HTTP でファイルダウンロードするには？
> <span class="sub">how to download a file via http from a url and with some headers in python?</span>

<div style="zoom: 60%">

> ChatGPT (GPT-4):
> To download a file from a URL with specific headers in Python, you can use the `requests` library. Here's an example:

```python
import requests

# The URL of the file you want to download
url = 'http://example.com/file.txt'

# The headers you want to use
headers = {
    'User-Agent': 'My User Agent 1.0',
    'From': 'youremail@example.com'  # This is another valid field
}

# Send a GET request to the URL
response = requests.get(url, headers=headers)

# Check that the request was successful
if response.status_code == 200:
    # Open a file in write mode and write the content to it
    with open('file.txt', 'wb') as file:
        file.write(response.content)
else:
    print(f'Request failed with status code {response.status_code}')
```

</div>

---

## とっかかりのない情報について概要を教えてもらう

### 例: Go 言語で Excel VBA ファイル(`*.xlsm`)のコード内容を取得したい

> Me: Excel マクロファイルを Go 言語でパースして VBA のソースコードを抽出したい
> <span class="sub">I want to parse Excel macro files (`*.xlsm`) with Go lang and extract VBA source code inside a file.</span>

<div style="zoom: 50%">

> ChatGPT (GPT-4):
> Parsing Excel macro files (.xlsm) to extract the VBA source code is a two-step process:
> Unzip the .xlsm file, because it is actually a ZIP file with a different extension.
> Parse the vbaProject.bin binary file within the unzipped contents. This is where the VBA code is stored.
> The first step is simple, as Go has a built-in package for handling ZIP files.
> The second step is a bit more involved, as vbaProject.bin is in a binary format called **Compound File Binary Format (CFBF)**, a legacy format from Microsoft. You will need to find or create a library that can handle this format.
> Here's a simple example for the first step:

```go
package main

import (
	"archive/zip"
	"fmt"
// 以下略
```

</div>

> Me: CFBF ファイルについてのドキュメントある？
> <span class="sub">Any docs about CFBF file?</span>

<div style="zoom: 50%">

> ChatGPT (GPT-4):
> Yes, Microsoft provides the official documentation for the Compound File Binary Format (CFBF), which is used by several Microsoft Office file formats including vbaProject.bin in .xlsm files.
> The [documentation](https://learn.microsoft.com/en-us/openspecs/windows_protocols/ms-cfb/53989ce4-7b05-4f8d-829b-d08d6148375b) includes a detailed specification of the CFBF file structure. This includes descriptions of the different types of sectors, directory entries, and the structure of streams and storages.

</div>

解決には至らないものの **CFBF というファイルフォーマットがあるという知見を得ることができた**

---

## 文章を書いてもらう

<div style="zoom: 70%">

```
下記点を含んだ顧客へのシステムメンテナンス通知を作ってください

- メンテナンス期間: 2023/06/27 23:00~24:00
- 新機能リリース及びシステムの保守
- たまにシステムに繋がらなくなる
- 作業完了次第、当社サービスサイトでお知らせする

```

```

件名：【重要】2023 年 6 月 27 日のシステムメンテナンスに関するお知らせ

親愛なるお客様へ,

この度は、弊社サービスをご利用いただき、誠にありがとうございます。
お客様にとってより良いサービスを提供できるよう、システムのメンテナンスおよび新機能のリリースを行います。
これに伴い、下記の日時にシステムを一時的にご利用いただけない時間帯が発生いたします。

【メンテナンス期間】

2023 年 6 月 27 日 23:00 時から 24:00 時まで（1 時間）
【メンテナンス内容】

システム全般の保守作業
新機能のリリース
【影響】

メンテナンス期間中、システムへのアクセスがたまに繋がらなくなる可能性がございます。これによるご不便をおかけすることを深くお詫び申し上げます。
【作業完了について】

メンテナンス作業が完了次第、弊社サービスサイト上でお知らせいたします。ご利用の皆様には、最新の情報を速やかにお伝えするよう努めます。
弊社としましては、お客様に安心してご利用いただけるサービスを提供するため、常にシステムの改善と向上に努めております。今回のメンテナンスにより、サービスの品質が向上することを確信しております。

ご理解とご協力を賜り、誠にありがとうございます。何かご不明な点がございましたら、お気軽に弊社サポートまでお問い合わせください。

敬具,

[会社名]
[部署名]
[お問い合わせ先のメールアドレスや電話番号]

```

</div>

---

## 英語を添削してもらう

> Please provide a detailed information about the issue you are facing.
> After receiving that, I register an issue ticket to our ITS and maintain it to be resolved.

↓

> Please provide detailed information about the issue you are facing. After I receive that, I will register an issue ticket in our ITS and ensure that it is being addressed.

![bg right fit](images/2023-06-15-19-30-35.png)

---

## コンサルタントして活用する

![bg right fit](images/2023-06-15-19-14-55.png)

---

## 記事を要約する / 日本語訳する

[記事](https://research.aimultiple.com/chatgpt-use-cases/) / [要約](https://chat.openai.com/share/b673755e-57f9-484c-9cbc-28f217d1d35e)

![bg right fit](images/2023-06-15-20-37-15.png)

---

## グラフを生成する

- プラグイン機能で [Wolfram Alpha](https://www.wolframalpha.com/) を有効にした状態

![bg right fit](images/2023-06-15-20-41-49.png)

---

## まとめ

- まるで **小さな師匠（弟子）** のように使える
- **全く知らない分野の足がかり**に使う
- より良い精度を出すために
  - **英語**を使う
  - **GPT-4** を使う
- 古い情報・**不正確な情報に気をつける**
  - 情報は裏を取る・全て正確とは思わない
- あくまで膨大なテキストを学習した末の**統計的確率をベースにした計算機がそれらしい応答をしているだけ**
- **機微情報はパブリックなサービスにアップロードしない**
  - <span class="sub">NG: 「この名簿データを出身地別にいい感じに CSV にして」</span>
  - クローズドな AI 環境ができれば、いつか実現はできそう

---

<!-- _class: primary_section -->

# 本日のまとめ

---

## 避けられない時代動向 〜 多能工化

- 我々の職業が脅かされるのでは？
- **多能工化** の傾向の一つと捉えられる
  - **複数の工程を少人数で兼任する**
  - コンビニは昔より担当するタスクの種類が増えた
  - YouTuber は企画・演出・撮影・技術・編集をすべて兼任していることが多い
- IT 技術者の多能工化を支えてきた技術
  - 近代文化の普及・コンピュータの普及
  - クラウド
  - モダン技術, DevOps など
- **未来のエンジニアは AI の補助を受けながら多種多様多量のタスクをこなすはず（？）**

---

## AI で変わる未来

- **AI による案内が中心化** 、現在の検索や索引が置き換えられる
  - マニュアルやウェブサイトなどの素材を既成 LLM AI モデルの **ファインチューニング（最終調整）** 材料として用いて、製品を学習した AI に顧客の案内をしてもらう
  - 索引の整備より、AI に学習させるデータ、AI にベースとして与えるデータの整備が中心に
- **AI による状況判断が当たり前に**
  - 家電のスイッチを一切さわらない時代が来るかも
  - サーバ環境ではメトリクスと障害の関連性の推論について機械が代行することが中心的に
  - 健康管理も人間が真似できないレベルでの因果の推論を行うかもしれない
- **テキスト、アセット（絵やスクリプト）の自動生成化**
  - AI によるブログの自動生成
  - メールやチャット等の返事の自動生成・補完
  - ゲームに登場する品質優先度の低いアセットは AI 生成に切り替わっていく
    - 巨大なゲームが少人数で制作可能に
    - ゲーム中にリアルタイム生成することも考えられる
- **ディープフェイク技術** の高度化・常識化
  - 映像が記録として信じられない時代に突入

---

## 最後に

本当に日々進歩しているので、**このスライドの内容はそのうち時代遅れ** になってしまいますが、**AI が現時点で生産性に寄与することは疑いようのない事実**です。
静観しているとそのうちついていけなくなるので、**こまめにキャッチアップ** していきましょう。

## 参考資料

日夜いろんな資料が出てくる……

- [ChatGPT の検索結果](https://b.hatena.ne.jp/q/ChatGPT?target=text&sort=recent)
- [ジェネレーティブ AI 実践入門/20230524](https://speakerdeck.com/yoshidashingo/20230524)
- [20230614\_「ChatGPT について調べてくれ」と社長から特命を受けた人のための ChatGPT 概論(40min 版)\_v1.00.pdf](https://speakerdeck.com/doradora09/20230614-chatgptnituitediao-hetekure-toshe-chang-karate-ming-woshou-ketaren-notamenochatgptgai-lun-40minban-v1-dot-00)

---

<!-- _class: primary_section -->

# 質問への回答

---

## ChatGPT の回答が途中で途切れる

- 有料版/無料版関係なく **長い回答になる場合は ChatGPT の回答が途中で途切れる** ことがあります
- その場合は 「**cont**」 (continue) あるいは 「**続けて**」 などと入力すると続きを返してくれます
  - 日本語英語関係なく **`cont` が短くオススメ**です
- （たまにエラーで回答が死ぬ場合がありますが、この場合は `Regenerate response` するしかありません）

---

## 新卒等、能力やリテラシーが未発達な者が AI を使った場合、真贋鑑定で最終的にコストが高くつくのでは？

- 調査のとっかかりや参考意見洗い出しの **道具** としては万人が便利に使えるはずだと思います
  - AI による偽証や一見それらしい回答があることによるバイアスがあることを、利用する側がきちんと認識して使う、**補助的な道具という位置づけ** でなければならないと考えます
  - 知らない分野について判断を誤らないためには、結局、**情報ソースや高速なとっかかりの一部として AI の回答を用いるものの、最終的にはその説を補強する信頼できる情報源を検索する or 実証する** などしなければなりません
    - その情報が誤っていた際のダメージを想定しているか？ それに基づいたコストタイムパフォーマンス、その辺を意識できるリテラシーの問題かなあと考えています
- 結論としては、**便利な道具をポジティブに使えるようにリテラシー教育するしかない**ということになりそうです
- AI によるアドバイス → 人のアドバイス → 信頼できる情報源のアドバイス という流れで、AI は **「人のアドバイス」の一部** として位置づけるのが良いと思います (by GitHub Copilot 氏)
- [Perplexity](https://www.perplexity.ai/) といった、AI に情報源を提示させる方向性のサービスも存在しています
  - AI に情報ソースを尋ねるのは割りとメジャーです

---

## 業務プログラミングと GitHub Copilot の相性について

バックグラウンドが LLM（大規模言語モデル） なので、汎用的なアルゴリズムを記述する、汎用的なイディオムを記述するといったタスク以外でも割りと空気を読んで人間的に補完してくれます。

簡単な例でいえば、

```
foo_bar="${FOO_BAR_PARAM}"
bar_baz=
```

から `"${BAR_BAZ_PARAM}"` を補完として導いてくれたりします。（あとはコメントをコードから提案したりなど）

---

# おわり

¡Hasta la vista!
