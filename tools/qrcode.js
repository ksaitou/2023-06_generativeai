const path = require("path");
const qrcode = require("qrcode");

const projectdir = (...paths) => path.join(__dirname, "../", ...paths);

qrcode.toFile(
  projectdir("slides/images/qrcode.png"),
  "https://ksaitou.gitlab.io/2023-06_generativeai/"
);
