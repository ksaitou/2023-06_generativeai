const path = require("path");
const fs = require("fs-extra");

const projectdir = (...paths) => path.join(__dirname, "../", ...paths);

if (fs.existsSync("public")) {
  fs.rmdirSync("public", { force: true, recursive: true });
}
fs.mkdirSync(projectdir("public"));
fs.copySync(projectdir("slides/images"), projectdir("public/images"));
