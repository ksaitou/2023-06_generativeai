# 2023-06 生成系 AI キャッチアップ会

- スライド: https://ksaitou.gitlab.io/2023-06_generativeai/
- リポジトリ: https://gitlab.com/ksaitou/2023-06_generativeai

## スライドの作り方

### スライドの開発

```
# 依存性取得
$ yarn

# サーバ: http://localhost:8080
$ yarn start

# HTMLで出力
$ yarn build
```

### 参考

- marp (プレゼンツール) - https://github.com/marp-team/marp-cli
  - https://marpit.marp.app/markdown
